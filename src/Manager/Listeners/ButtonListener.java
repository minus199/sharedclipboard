package Manager.Listeners;

import Manager.Manager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

abstract public class ButtonListener implements ActionListener {
    protected Manager caller;

    public ButtonListener(Manager caller) {
        this.caller = caller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JLabel) {

            System.out.println("label was clicked");
        } else if (e.getSource() instanceof JButton) {
            System.out.println("e = " + e);
        } else {
            System.out.println("e = " + e);
        }
    }
}
