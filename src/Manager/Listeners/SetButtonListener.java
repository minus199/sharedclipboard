package Manager.Listeners;

import Manager.Manager;

import java.awt.event.ActionEvent;

/**
 * Created by minus on 7/17/15.
 */
public class SetButtonListener extends ButtonListener{
    public SetButtonListener(Manager caller) {
        super(caller);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("e = " + e);
        super.actionPerformed(e);
        String textToClipboard = this.caller.getMenuMaker().getTextArea().getText();
        this.caller.copy(textToClipboard);
    }
}
