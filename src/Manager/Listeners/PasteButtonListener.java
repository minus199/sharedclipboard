package Manager.Listeners;

import Manager.Manager;
import com.intellij.ui.components.JBLabel;

import javax.swing.*;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.IOException;

/**
 * Created by minus on 7/17/15.
 */
public class PasteButtonListener extends ButtonListener {
    public PasteButtonListener(Manager caller) {
        super(caller);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JButton){
            JButton button = (JButton) e.getSource();
            JLabel label = this.caller.getMenuMaker().getLabelByButton(button).get(0);

            try {
                this.caller.getMenuMaker().getTextArea().setText(this.caller.get());
            } catch (UnsupportedFlavorException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
