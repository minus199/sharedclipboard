package Manager;

import Manager.Listeners.ButtonListener;
import Manager.Listeners.PasteButtonListener;
import Manager.Listeners.SetButtonListener;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;


public class Manager {
    JLabel l;
    MenuMaker menuMaker;

    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.createMenu();

        Component[] components = manager.menuMaker.getPanel().getComponents();


		/*try {
			Image dataa = (Image) Toolkit.getDefaultToolkit()
					.getSystemClipboard().getData(DataFlavor.imageFlavor);
			System.out.println("Current content: " + dataa);
		} catch (HeadlessException | UnsupportedFlavorException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    }

    private Manager createMenu(){
        menuMaker = new MenuMaker();
        menuMaker
                .createTextArea()
                .createButton("Set", new SetButtonListener(this))
                .createButton("Get", new PasteButtonListener(this))
                .createButton("Clear", new PasteButtonListener(this))
            .done()
        ;

        return this;
    }

    public void paste1()
    {
        try{
            menuMaker.createLabel(new ImageIcon((BufferedImage) Toolkit
                    .getDefaultToolkit().getSystemClipboard()
                    .getData(DataFlavor.imageFlavor)));
        } catch (Exception e) {
            // If image is not found, try text!
            try
            {
                // Create a JLabel containing text that is in the clipboard
                menuMaker.createLabel((String) Toolkit.getDefaultToolkit()
                        .getSystemClipboard().getData(DataFlavor.stringFlavor));
            } catch (Exception e1) {
                menuMaker.createLabel("Nothing is there in the clipboard!");
            }
        }
        Component componentCount = menuMaker.getPanel().getComponent(1);

        System.out.printf(componentCount.getAccessibleContext().toString());
        menuMaker.done();

    }

    public void copy(String text)
    {
        Clipboard clipboard = this.getSystemClipboard();
        clipboard.setContents(new StringSelection(text), null);
    }

    public void paste() throws AWTException
    {
        Robot robot = new Robot();

        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_V);
    }

    public String get() throws HeadlessException,
            UnsupportedFlavorException, IOException
    {
        Clipboard systemClipboard = getSystemClipboard();
        Object text = systemClipboard.getData(DataFlavor.stringFlavor);

        return (String) text;
    }

    private Clipboard getSystemClipboard()
    {
        Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
        return defaultToolkit.getSystemClipboard();
    }

    public MenuMaker getMenuMaker() {
        return menuMaker;
    }
}