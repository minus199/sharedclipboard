package Manager;

import com.intellij.util.containers.BidirectionalMap;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by minus on 7/14/15.
 */
public class MenuMaker {
    private JPanel panel;
    private JFrame frame;

    private BidirectionalMap<JLabel, JButton> buttons;
    private BidirectionalMap<JLabel, JTextField> textFields;
    private JTextArea textArea;

    public MenuMaker(){
        buttons = new BidirectionalMap<JLabel, JButton>();
        textFields = new BidirectionalMap<JLabel, JTextField>();

        frame = new JFrame();
        frame.setSize(700, 200);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        panel = new JPanel();
        frame.add(panel);

    }

    public JPanel getPanel(){
        return panel;
    }

    public JFrame done(){
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);

        return frame;
    }

    public MenuMaker createButton(String name, ActionListener actionListener){
        JButton button = new JButton(name);
        button.setBounds(10, 80, 80, 25);
        button.addActionListener(actionListener);

        JLabel label = createLabel(name);
        label.setVisible(false);
        buttons.put(label, button);
        panel.add(button);

        return this;
    }

    public MenuMaker createTextField(String name, Boolean isPassword, ActionListener actionListener){
        createLabel(name);

        JTextField textField = new JTextField();
        textField.addActionListener(actionListener);
        textField.setBounds(100, 10, 160, 25);

        panel.add(textField);

        return this;
    }

    public MenuMaker createPasswordField(){
        createLabel("Password");

        JPasswordField passwordText = new JPasswordField(20);
        passwordText.setBounds(100, 40, 160, 25);
        panel.add(passwordText);

        return this;
    }

    public MenuMaker createTextField(String name, ActionListener actionListener){
        return createTextField(name, true, actionListener);
    }

    public MenuMaker createTextArea(){
        textArea = new JTextArea();
        textArea.setBounds(0,0, 500, 500);
        textArea.setColumns(50);
        textArea.setRows(50);

        panel.add(textArea);

        return this;
    }

    private JLabel createLabel(JLabel label){
        label.setBounds(10, 10, 80, 25);
        panel.add(label);

        return label;
    }

    public JLabel createLabel(ImageIcon imageIcon){
        return createLabel(new JLabel(imageIcon));
    }

    public JLabel createLabel(String text){
        return createLabel(new JLabel(text));
    }

    public BidirectionalMap<JLabel, JTextField> getTextFields() {
        return textFields;
    }

    public BidirectionalMap<JLabel, JButton> getButtons() {
        return buttons;
    }

    public JButton getButtonByLabel(JLabel label){
        return this.getButtons().get(label);
    }

    public List<JLabel> getLabelByButton(JButton button){
        return this.getButtons().getKeysByValue(button);
    }

    public MenuMaker setButtons(JLabel label, JButton button) {
        buttons.put(label, button);
        return this;
    }

    public MenuMaker setTextFields(JLabel label, JTextField textField) {
        textFields.put(label, textField);

        return this;
    }

    public JTextArea getTextArea() {
        return textArea;
    }
}
